
package jogodacobrinha;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class CobraJPanel extends JPanel implements ActionListener {
    private final int TAMANHO_CELULA = 10;
    private final String PATH_ICONE_CABECA = "src/resources/cabeca.png";
    private final String PATH_ICONE_CORPO = "src/resources/corpo.png";

       
    //private Boolean jogoRolando = true;
    private Image imagemCabeca;
    private Image imagemCorpo;
    
    private int posicaoIconeCabecaX = 0;
    private int posicaoIconeCabecaY = 0;
    
    private int posicaoIconeCorpoX = posicaoIconeCabecaX - 12;
    private int posicaoIconeCorpoY = posicaoIconeCabecaY;
    
    private int ultimaTecla;
    
    CobraCobrinha coordenadaAtual;
    
    public CobraJPanel(int numPixelsLinhas, int numPixelsColunas) {       
        addKeyListener(new TAdapter());
        
        setBackground(Color.black);
        setFocusable(true);

        setPreferredSize(new Dimension(numPixelsLinhas, numPixelsColunas));
        
        ImageIcon iconeCabeca = new ImageIcon(PATH_ICONE_CABECA);
        imagemCabeca = iconeCabeca.getImage();  
        
        ImageIcon iconeCorpo = new ImageIcon(PATH_ICONE_CORPO);
        imagemCorpo = iconeCorpo.getImage();
    }
    
    public int getultimaTecla() {
        return ultimaTecla;
    }
            
    public void MoverIconeCobrinha(CobraCobrinha coordenadaNova) {
        posicaoIconeCabecaX = coordenadaNova.getCoordenada().getLinha();
        posicaoIconeCabecaY = coordenadaNova.getCoordenada().getColuna();
        
        posicaoIconeCorpoX = coordenadaNova.getCoordenada().getLinha();
        posicaoIconeCorpoY = coordenadaNova.getCoordenada().getColuna() - 12;
    }
            
    @Override
    public void actionPerformed(ActionEvent e) {
    }
    
    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        if (imagemCabeca != null){
            g.drawImage(imagemCabeca, posicaoIconeCabecaY, posicaoIconeCabecaX, this);            
        }        
        //super.paintComponent(g);
        if (imagemCorpo != null){
            g.drawImage(imagemCorpo, posicaoIconeCorpoY, posicaoIconeCorpoX, this);            
        }        
    }    
     
    private class TAdapter extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {

            ultimaTecla = e.getKeyCode();
        }
    }   
}
