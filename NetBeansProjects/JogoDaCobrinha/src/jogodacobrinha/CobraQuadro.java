package jogodacobrinha;

public class CobraQuadro {
    int numLinhas, numColunas; 
    private CobraQuadroCelula[][] celulas;
    
    public CobraQuadro(int numLinhas, int numColunas) 
    { 
        this.numLinhas = numLinhas; 
        this.numColunas = numColunas; 
  
        celulas = new CobraQuadroCelula[numLinhas][numColunas]; 
        for (int linha = 0; linha < numLinhas; linha++) { 
            for (int coluna = 0; coluna < numColunas; coluna++) { 
                celulas[linha][coluna] = new CobraQuadroCelula(linha, coluna); 
                celulas[linha][coluna].setQuadroCelulaTipo(CobraQuadroCelula.QuadroCelulaTipo.VAZIO);
            } 
        } 
    } 
    
    public CobraQuadroCelula[][] getCelulas() 
    { 
        return celulas; 
    } 
    
    public void setCobraCabeca(CobraQuadroCelula cabecaCobra) 
    {
        celulas[cabecaCobra.getLinha()][cabecaCobra.getColuna()].setQuadroCelulaTipo(CobraQuadroCelula.QuadroCelulaTipo.CABECA_COBRA);
    }
}
