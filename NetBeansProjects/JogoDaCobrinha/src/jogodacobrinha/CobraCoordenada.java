package jogodacobrinha;

public class CobraCoordenada {
    private int linha;
    private int coluna;
    
    public CobraCoordenada() {
    }
    
    public CobraCoordenada(int linha, int coluna) {
        this.linha = linha;
        this.coluna = coluna;
    }
            
    public int setLinha(int linha) {
        this.linha = linha;
        return linha;                   
    }
    public int getLinha() {
        return linha;
    }
    public int setColuna (int coluna) {
        this.coluna = coluna;
        return coluna;
    }
    public int getColuna() {
        return coluna;
    }
}