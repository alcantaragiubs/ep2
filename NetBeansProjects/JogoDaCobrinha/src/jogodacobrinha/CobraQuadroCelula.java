package jogodacobrinha;

public class CobraQuadroCelula { 
    
    private final int linha;
    private final int coluna; 
    private QuadroCelulaTipo quadroCelulaTipo; 
  
    public CobraQuadroCelula(int linha, int coluna) 
    { 
        this.linha = linha; 
        this.coluna = coluna;         
    } 
  
    public QuadroCelulaTipo getQuadroCelulaTipo() 
    { 
        return quadroCelulaTipo; 
    } 
  
    public void setQuadroCelulaTipo(QuadroCelulaTipo quadroCelulaTipo) 
    { 
        this.quadroCelulaTipo = quadroCelulaTipo; 
    } 
  
    public int getLinha() 
    { 
        return linha; 
    } 
  
    public int getColuna() 
    { 
        return coluna; 
    } 

    public enum QuadroCelulaTipo {   
        VAZIO, 
        FRUTA, 
        CABECA_COBRA,
        PEDACO_COBRA; 
    } 
}

