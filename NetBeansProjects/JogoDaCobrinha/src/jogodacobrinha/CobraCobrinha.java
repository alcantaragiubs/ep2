package jogodacobrinha;

public class CobraCobrinha {
    private int tamanho_celula = 10;
    
    private Direcao direcaoAtual = Direcao.DIREITA;
    
    CobraCoordenada coordenadaAtual;

    public CobraCobrinha(CobraCoordenada coordenada, int tamanho_celula) 
    { 
        this.coordenadaAtual = coordenada;
        this.tamanho_celula = tamanho_celula;
    } 
    
    public CobraCoordenada getCoordenada() {
        return coordenadaAtual;
    }

    private void MoverPara(CobraCoordenada coordenada) 
    { 
        this.coordenadaAtual = coordenada;
    } 
    
    public Direcao getDirecaoAtual() {
        return direcaoAtual;
    }

    public void Mover() 
    {
        Mover(direcaoAtual);
        return;
    }
    
    public void Mover(Direcao direcaoProxima) 
    { 
            CobraCoordenada novaCoordenada = new CobraCoordenada();
            if ((direcaoProxima == Direcao.ESQUERDA) && (direcaoAtual != Direcao.DIREITA)) {
                novaCoordenada.setColuna(coordenadaAtual.getColuna() - tamanho_celula);
                novaCoordenada.setLinha(coordenadaAtual.getLinha());
                MoverPara(novaCoordenada);
                direcaoAtual = Direcao.ESQUERDA;
            } else if ((direcaoProxima == Direcao.DIREITA) && (direcaoAtual != Direcao.ESQUERDA)) {
                novaCoordenada.setColuna(coordenadaAtual.getColuna() + tamanho_celula);
                novaCoordenada.setLinha(coordenadaAtual.getLinha());
                MoverPara(novaCoordenada);
                direcaoAtual = Direcao.DIREITA;
            } else if ((direcaoProxima == Direcao.CIMA) && (direcaoAtual != Direcao.BAIXO)) {
                novaCoordenada.setLinha(coordenadaAtual.getLinha() - tamanho_celula);
                novaCoordenada.setColuna(coordenadaAtual.getColuna());
                MoverPara(novaCoordenada);
                direcaoAtual = Direcao.CIMA;                
            } else if ((direcaoProxima == Direcao.BAIXO) && (direcaoAtual != Direcao.CIMA)) {
                novaCoordenada.setLinha(coordenadaAtual.getLinha() + tamanho_celula);
                novaCoordenada.setColuna(coordenadaAtual.getColuna());
                MoverPara(novaCoordenada);
                direcaoAtual = Direcao.BAIXO;
            } else {
                Mover();
            }               
    } 

    public enum Direcao { 
        ESQUERDA, 
        DIREITA, 
        CIMA,
        BAIXO
    } 
}
