package jogodacobrinha;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class CobraJFrame extends JFrame { 
    //private CobraJPanel cobraJPanel;
    
    //public CobraJFrame(int numPixelsLinhas, int numPixelsColunas) {    
    public CobraJFrame(CobraJPanel cobraJPanel) {    
        //cobraJPanel = new CobraJPanel(numPixelsLinhas,numPixelsColunas);
        add(cobraJPanel);
               
        setResizable(false);
        pack();
        
        setTitle("Jogo da Cobrinha");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
