package jogodacobrinha;

import java.awt.event.KeyEvent;
import javax.swing.JPanel;
import javax.swing.text.Keymap;
import jogodacobrinha.CobraCobrinha.Direcao;

public class CobraJogo {
    final static int PAINEL_PIXELS_LINHAS = 1000;  //multiplo de 10
    final static int PAINEL_PIXELS_COLUNAS = 1000; //multiplo de 10
    
    final static int COBRA_LINHA_INICIAL = 100;
    final static int COBRA_COLUNA_INICIAL = 100; 
    final static int COBRA_TAM_CELULA = 10;
    
    private CobraCobrinha cobrinha;

    private Boolean jogoRolando = true;

    private CobraJPanel cobraJPanel;
    
    CobraCoordenada coordenadaIncial;

    public CobraJogo() {
        CobraJPanel cobraJPanel = new CobraJPanel(PAINEL_PIXELS_LINHAS,PAINEL_PIXELS_COLUNAS);
        
        CobraJFrame cobraJFrame = new CobraJFrame(cobraJPanel);
        cobraJFrame.setVisible(true); 
        cobraJPanel.setFocusable(true);
        coordenadaIncial = new CobraCoordenada(COBRA_LINHA_INICIAL, COBRA_COLUNA_INICIAL);
        this.cobraJPanel = cobraJPanel;
        cobrinha = new CobraCobrinha(coordenadaIncial,COBRA_TAM_CELULA);        
    }
    
    private CobraCobrinha.Direcao TransformarTeclaEmDirecao(int teclaPressionada) {
        if (teclaPressionada == KeyEvent.VK_LEFT) {
            return Direcao.ESQUERDA;
        } else if (teclaPressionada == KeyEvent.VK_RIGHT) {
            return Direcao.DIREITA;
        } else if (teclaPressionada == KeyEvent.VK_UP) {
            return Direcao.CIMA;
        } else if (teclaPressionada == KeyEvent.VK_DOWN) {
            return Direcao.BAIXO; 
        }
        return null;
    }
    
    public void Iniciar() throws InterruptedException {        
        while (true) {
            Thread.sleep(100);
            CobraCobrinha.Direcao ultimaDirecaoTeclada = TransformarTeclaEmDirecao(cobraJPanel.getultimaTecla());
            if (ultimaDirecaoTeclada == null) {
                ultimaDirecaoTeclada = cobrinha.getDirecaoAtual();
            }
            if (ultimaDirecaoTeclada != cobrinha.getDirecaoAtual()) {
                cobrinha.Mover(ultimaDirecaoTeclada);
            } else {
                cobrinha.Mover();
            }            
            cobraJPanel.MoverIconeCobrinha(cobrinha);
            cobraJPanel.repaint();
        }
    }           
}
