package jogodacobrinha;

import java.awt.EventQueue;

public class CobraMain {
    final static int PAINEL_PIXELS_LINHAS = 1000;  //multiplo de 10
    final static int PAINEL_PIXELS_COLUNAS = 1000; //multiplo de 10
     
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            CobraJFrame cobraJFrame = new CobraJFrame(PAINEL_PIXELS_LINHAS,PAINEL_PIXELS_COLUNAS);            
            cobraJFrame.setVisible(true);           
        });
    }      
}
